#! /bin/sh
xhost +
docker run -it --rm \
	--name primary_gui \
	--env BATCH_SAVE_DIR="/opt/batches" \
	--env QSERVER_ZMQ_CONTROL_ADDRESS=tcp://localhost:60615 \
	--env QSERVER_ZMQ_INFO_ADDRESS=tcp://localhost:60625 \
	--env DISPLAY=${DISPLAY} \
	--network host \
	-v /home/myspot/bluesky/bluesky_gui/batches:/opt/batches \
	-v /tmp/.X11-unix:/tmp/.X11-unix:rw \
	registry.hzdr.de/hzb/bluesky/qt_gui/images/minimal-qt-gui-image:latest \
	bluesky-emil-gui --title "Sample Environment" --zmq localhost:5578
