#!/bin/bash

# env definitions
RED='\033[0;31m'
NC='\033[0m' # No Color

# Display usage instructions
show_help() {
	echo "Usage: ./script.sh [command] [--name container_name] [--tag container_tag]"
	echo "Start the specified container as root"
	echo "  command: Command to run inside the container (sh, stop, or any command, defaults to 'bluesky_start_root')"
	echo "  --name: Name of the container to run (bluesky_container or re_manager, defaults to bluesky_container)"
	echo "  --tag: Optional container tag (defaults to latest)"
}

# Check for help option
if [[ "$1" == "-h" || "$1" == "--h" || "$1" == "-help" || "$1" == "--help" || "$1" == "help" || "$1" == "h" ]]; then
	show_help
	exit 0
fi

# Fix the host's X11 permissions
xhost +

# Define default values
default_container="bluesky_container"
default_tag=$BLUESKY_CONTAINER_TAG
default_command="bluesky_start_root"

# Function to deploy container
deploy_container() {
	local command=$1
	local container_name=$2
	local tag=$3
	docker run -it --name "$container_name" \
		--env DISPLAY=${DISPLAY} \
		--env TILED_URL=${TILED_URL} \
		--env TILED_API_KEY=${TILED_API_KEY} \
		--env ZMQ_URL=${ZMQ_URL} \
		--env _DEV_CFG_FILE_DIR_PATH=${_DEV_CFG_FILE_DIR_PATH} \
		--env _RML_FILE_DIR_PATH=${_RML_FILE_DIR_PATH} \
		--env _HAPPI_DB_FILE_DIR_PATH=${_HAPPI_DB_FILE_DIR_PATH} \
		--env _DEV_INSTANTIATION_SETUP_FILE_DIR_PATH=${_DEV_INSTANTIATION_SETUP_FILE_DIR_PATH} \
		--env DEV_INSTANTIATION_SETUP_FILE_NAME=${DEV_INSTANTIATION_SETUP_FILE_NAME} \
		--env _NX_FILE_DIR_PATH=${_NX_FILE_DIR_PATH} \
		--env _NX_LOG_FILE_DIR_PATH=${_NX_LOG_FILE_DIR_PATH} \
		--env _NX_SCHEMA_DIR_PATH=${_NX_SCHEMA_DIR_PATH} \
		--network host \
		-v /tmp/.X11-unix:/tmp/.X11-unix \
		-v ~/.Xauthority:/root/.Xauthority \
		-v ${BEAMLINETOOLS_PATH}:/opt/bluesky/beamlinetools \
		-v ${DATA_PATH}:/opt/bluesky/data \
		-v ${USERSCRIPTS_PATH}:/opt/bluesky/user_scripts \
		-v ${HISTORY_PATH}/history.sqlite:/opt/bluesky/ipython/profile_root/history.sqlite \
		-v ${DEV_CFG_FILE_DIR_PATH}:${_DEV_CFG_FILE_DIR_PATH} \
		-v ${RML_FILE_DIR_PATH}:${_RML_FILE_DIR_PATH} \
		-v ${HAPPI_DB_FILE_DIR_PATH}:${_HAPPI_DB_FILE_DIR_PATH} \
		-v ${DEV_INSTANTIATION_SETUP_FILE_DIR_PATH}:${_DEV_INSTANTIATION_SETUP_FILE_DIR_PATH} \
		-v ${NX_FILE_DIR_PATH}:${_NX_FILE_DIR_PATH} \
		-v ${NX_LOG_FILE_DIR_PATH}:${_NX_LOG_FILE_DIR_PATH} \
		-v ${NX_SCHEMA_DIR_PATH}:${_NX_SCHEMA_DIR_PATH} \
		-v /etc/localtime:/etc/localtime:ro \
		-v /etc/timezone:/etc/timezone:ro \
		--device=/dev/dri:/dev/dri \
		"registry.hzdr.de/hzb/bluesky/core/images/bluesky_container:${tag}" \
		sh -c "python3 -m pip install -e /opt/bluesky/beamlinetools && $command"
}

# Cleanup function
cleanup() {
	local container_name=$1
	echo "Removing container $container_name and exiting..."
	echo -e "${RED}If it was not intentional, just restart the container.${NC}"
	docker container stop "$container_name" >/dev/null 2>&1 && docker container rm "$container_name" >/dev/null 2>&1
	# Add further cleanup steps if necessary
	exit 1
}

# Trap Ctrl+C and call the cleanup function
trap 'cleanup "$container_name"' EXIT

# Initialize default values
container_name="$default_container"
tag="$default_tag"
command_to_run="$default_command"

# Parse arguments
while [[ $# -gt 0 ]]; do
	case $1 in
	--name)
		container_name="$2"
		shift 2
		;;
	--tag)
		tag="$2"
		shift 2
		;;
	*)
		if [[ -z "$command_to_run" ]]; then
			command_to_run="$1"
			shift
		else
			echo "Unexpected argument: $1"
			show_help
			exit 1
		fi
		;;
	esac
done

# Check if command is valid
if [[ "$command_to_run" != "sh" && "$command_to_run" != "stop" && "$command_to_run" != "$default_command" ]]; then
	echo "Invalid command: $command_to_run"
	show_help
	exit 1
fi

# Check if container name is valid
if [[ "$container_name" != "bluesky_container" && "$container_name" != "re_manager" ]]; then
	echo "Invalid container name: $container_name"
	show_help
	exit 1
fi

# Check command arguments and execute corresponding actions
case "$command_to_run" in
"stop")
	echo "Stopping and removing container $container_name"
	cleanup "$container_name"
	;;
"sh")
	deploy_container sh "$container_name" "$tag"
	;;
*)
	# If command is not 'stop' or 'sh', assume it's a command to run
	deploy_container "$command_to_run" "$container_name" "$tag"
	;;
esac
