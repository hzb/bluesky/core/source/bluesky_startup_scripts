#! /bin/sh
xhost +
docker run -it --rm \
	--name flint \
	--env REDIS_DATA_HOST= ${REDIS_DATA_HOST} \
	--env DISPLAY=${DISPLAY} \
	--network host \
	-v /tmp/.X11-unix:/tmp/.X11-unix:rw \
	registry.hzdr.de/rock-it/wp2/flint_docker:latest
