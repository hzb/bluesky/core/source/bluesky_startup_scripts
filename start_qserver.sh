#! /bin/sh
cd /home/beamline_user/.bluesky-deployment/bluesky_queueserver
docker compose down

docker compose up -d
wait
date
echo "Done"
