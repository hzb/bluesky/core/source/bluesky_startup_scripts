# Bluesky Control System Scripts

This repository contains a collection of shell scripts for managing Bluesky and Phoebus containers in a beamline control system environment.

## Scripts Overview

### 1. run_bluesky.sh
The main script for running Bluesky containers with various configuration options.

**Usage:**
```bash
./run_bluesky.sh
```
Launches container in default mode (removed when stopped)

### Command Line Options

| Option | Description |
|--------|-------------|
| `-h, --help` | Display help information |
| `-r, --restart` | Launch with automatic restart policy |
| `-s, --settings` | Launch with additional settings.ini mounted |

### Examples
```bash
# Launch with help
./run_bluesky.sh --help

# Launch with restart enabled
./run_bluesky.sh --restart

# Launch with settings
./run_bluesky.sh --settings
```

## Container Configuration

### Mounted Volumes
- X11 socket and authority file for display
- Phoebus configuration directories
- User home directory and cache
- System files (passwd, group, shadow)
- Device drivers

### Environment Settings
- Display configuration
- User information
- Runtime directories
- Graphics settings

### Security
- Runs with user permissions
- AppArmor unconfined
- Host network access
- IPC host access

## Automatic Directory Creation
The script automatically creates and sets proper permissions for:
- User cache directory
- dconf cache directory
- Runtime directory

## Error Handling
- Checks for required `BEAMLINE_USER` environment variable
- Creates necessary directories with proper permissions
- Sets up proper user and group IDs

## Notes
- The container uses host networking
- Graphics acceleration is enabled through `/dev/dri` device mounting
- Container name is set to "Phoebus"
- Working directory is set to `/opt/phoebus/gui/`

## Registry
The container image is pulled from:
```
registry.hzdr.de/hzb/epics/phoebus-container
```