#!/bin/sh

xhost +

# Directory paths for Phoebus configuration
SCREENS_DIR="/opt/phoebus/gui"       # Directory containing GUI screens
MEMENTO="/opt/phoebus/.phoebus"      # Phoebus configuration directory
SETTINGS="/opt/phoebus/settings.ini" # Settings file path

# Environment variable BEAMLINE_USER should be set to specify the user
# who will own and run the Phoebus container

# This script launches a Docker container running Phoebus, a control system studio application
# The container can be started in different modes based on command line arguments:
# - Default mode: Container runs without restart policy and is removed when stopped
# - Restart mode: Container automatically restarts if it stops
# - Settings mode: Container mounts an additional settings.ini file

# Set default Phoebus tag if not specified
PHOEBUS_TAG=${PHOEBUS_TAG:-latest} # Use 'latest' if PHOEBUS_TAG not set

# Set default scale factor if not specified
PHOEBUS_SCALE=${PHOEBUS_SCALE:-1} # Use '1' if PHOEBUS_SCALE not set

# Set default container name if not specified
PHOEBUS_CONTAINER_NAME=${PHOEBUS_CONTAINER_NAME:-Phoebus} # Use 'Phoebus' if CONTAINER_NAME not set

# Check if required BEAMLINE_USER environment variable is set
if [ -z "$BEAMLINE_USER" ]; then # Check if BEAMLINE_USER is empty
	echo "Error: BEAMLINE_USER environment variable is not set"
	echo "Please set it with: export BEAMLINE_USER=<username>"
	exit 1 # Exit with error if not set
fi
USER_HOME="/home/$BEAMLINE_USER" # Set user's home directory path

# Create cache directories if they don't exist
if [ ! -d "$USER_HOME/.cache" ]; then                    # Check if main cache directory exists
	mkdir -p "$USER_HOME/.cache"                            # Create cache directory
	chown $BEAMLINE_USER:$BEAMLINE_USER "$USER_HOME/.cache" # Set ownership
fi
if [ ! -d "$USER_HOME/.cache/dconf" ]; then                    # Check if dconf cache directory exists
	mkdir -p "$USER_HOME/.cache/dconf"                            # Create dconf directory
	chown $BEAMLINE_USER:$BEAMLINE_USER "$USER_HOME/.cache/dconf" # Set ownership
fi

# Get user info for container
USER_ID=$(id -u $BEAMLINE_USER)                         # Get numeric user ID
GROUP_ID=$(id -g $BEAMLINE_USER)                        # Get numeric group ID
USER_NAME=$(getent passwd $BEAMLINE_USER | cut -d: -f1) # Get username
GROUP_NAME=$(getent group $GROUP_ID | cut -d: -f1)      # Get group name

# Create runtime directory if it doesn't exist
if [ ! -d "/run/user/$USER_ID" ]; then                    # Check if runtime directory exists
	mkdir -p "/run/user/$USER_ID"                            # Create runtime directory
	chown $BEAMLINE_USER:$BEAMLINE_USER "/run/user/$USER_ID" # Set ownership
fi

# Process command line arguments
case "$1" in
-h | --help)
	# Display help information about script usage
	echo "Usage: $0 [options]"
	echo "Options: -h, --help"
	echo "Restart: -r, --restart - start the container with the restart option"
	echo "Settings: -s, --settings - start container with additional settings.ini mounted"
	exit 0
	;;
-r | --restart)
	exec docker run -dit \
		--restart always \
		--network host \
		--security-opt apparmor=unconfined \
		--env DISPLAY=$DISPLAY \
		--env HOME=$USER_HOME \
		--env XDG_RUNTIME_DIR="/run/user/$USER_ID" \
		--env XDG_CACHE_HOME="$USER_HOME/.cache" \
		--env DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/$USER_ID/bus" \
		--env USER=$USER_NAME \
		--env USERNAME=$USER_NAME \
		--env USER_ID=$USER_ID \
		--env GROUP_ID=$GROUP_ID \
		--env GDK_SCALE=$PHOEBUS_SCALE \
		--env GDK_DPI_SCALE=1 \
		--env QT_X11_NO_MITSHM=1 \
		-v "$HOME/.Xauthority:$USER_HOME/.Xauthority:rw" \
		-v /tmp/.X11-unix:/tmp/.X11-unix:rw \
		-v $MEMENTO:/opt/phoebus/.phoebus \
		-v $SCREENS_DIR:/opt/phoebus/gui:rw \
		-v $USER_HOME:$USER_HOME \
		-v "$USER_HOME/.cache:$USER_HOME/.cache" \
		-v /run/user/$USER_ID:/run/user/$USER_ID \
		-v /etc/passwd:/etc/passwd:ro \
		-v /etc/group:/etc/group:ro \
		-v /etc/shadow:/etc/shadow:ro \
		-w /opt/phoebus/gui/ \
		--device=/dev/dri:/dev/dri \
		--ipc=host \
		--user $USER_ID:$GROUP_ID \
		--name $PHOEBUS_CONTAINER_NAME \
		registry.hzdr.de/hzb/epics/phoebus-container:$PHOEBUS_TAG \
		"$@"
	;;
-s | --settings)
	exec docker run -dit \
		--restart always \
		--network host \
		--security-opt apparmor=unconfined \
		--env DISPLAY=$DISPLAY \
		--env HOME=$USER_HOME \
		--env XDG_RUNTIME_DIR="/run/user/$USER_ID" \
		--env XDG_CACHE_HOME="$USER_HOME/.cache" \
		--env DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/$USER_ID/bus" \
		--env USER=$USER_NAME \
		--env USERNAME=$USER_NAME \
		--env USER_ID=$USER_ID \
		--env GROUP_ID=$GROUP_ID \
		--env GDK_SCALE=$PHOEBUS_SCALE \
		--env GDK_DPI_SCALE=1 \
		--env QT_X11_NO_MITSHM=1 \
		-v "$HOME/.Xauthority:$USER_HOME/.Xauthority:rw" \
		-v /tmp/.X11-unix:/tmp/.X11-unix:rw \
		-v $MEMENTO:/opt/phoebus/.phoebus \
		-v $SETTINGS:/opt/phoebus/settings.ini \
		-v $SCREENS_DIR:/opt/phoebus/gui:rw \
		-v $USER_HOME:$USER_HOME \
		-v "$USER_HOME/.cache:$USER_HOME/.cache" \
		-v /run/user/$USER_ID:/run/user/$USER_ID \
		-v /etc/passwd:/etc/passwd:ro \
		-v /etc/group:/etc/group:ro \
		-v /etc/shadow:/etc/shadow:ro \
		-w /opt/phoebus/gui/ \
		--device=/dev/dri:/dev/dri \
		--ipc=host \
		--user $USER_ID:$GROUP_ID \
		--name $PHOEBUS_CONTAINER_NAME \
		registry.hzdr.de/hzb/epics/phoebus-container:$PHOEBUS_TAG \
		"$@"
	;;
*)
	exec docker run -dit \
		--rm \
		--network host \
		--security-opt apparmor=unconfined \
		--env DISPLAY=$DISPLAY \
		--env HOME=$USER_HOME \
		--env XDG_RUNTIME_DIR="/run/user/$USER_ID" \
		--env XDG_CACHE_HOME="$USER_HOME/.cache" \
		--env DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/$USER_ID/bus" \
		--env USER=$USER_NAME \
		--env USERNAME=$USER_NAME \
		--env USER_ID=$USER_ID \
		--env GROUP_ID=$GROUP_ID \
		--env GDK_SCALE=$PHOEBUS_SCALE \
		--env GDK_DPI_SCALE=1 \
		--env QT_X11_NO_MITSHM=1 \
		-v "$HOME/.Xauthority:$USER_HOME/.Xauthority:rw" \
		-v /tmp/.X11-unix:/tmp/.X11-unix:rw \
		-v $MEMENTO:/opt/phoebus/.phoebus \
		-v $SCREENS_DIR:/opt/phoebus/gui:rw \
		-v $USER_HOME:$USER_HOME \
		-v "$USER_HOME/.cache:$USER_HOME/.cache" \
		-v /run/user/$USER_ID:/run/user/$USER_ID \
		-v /etc/passwd:/etc/passwd:ro \
		-v /etc/group:/etc/group:ro \
		-v /etc/shadow:/etc/shadow:ro \
		-w /opt/phoebus/gui/ \
		--device=/dev/dri:/dev/dri \
		--ipc=host \
		--user $USER_ID:$GROUP_ID \
		--name $PHOEBUS_CONTAINER_NAME \
		registry.hzdr.de/hzb/epics/phoebus-container:$PHOEBUS_TAG \
		"$@"
	;;
esac
