#! /bin/sh

qserver-list-plans-devices --startup-dir /opt/bluesky/ipython/profile_root/startup
/usr/local/bin/start-re-manager --startup-dir /opt/bluesky/ipython/profile_root/startup --existing-plans-devices=./existing_plans_and_devices.yaml --user-group-permissions=/opt/user_group_permissions.yaml --keep-re --redis-addr localhost:6379 --zmq-publish-console ON
