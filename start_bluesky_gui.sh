#! /bin/sh
xhost +
docker run -it --rm --env DISPLAY=${DISPLAY} --network host -v /tmp/.X11-unix:/tmp/.X11-unix registry.hzdr.de/hzb/bluesky/qt_gui/images/minimal-qt-gui-image:${BLUESKY_GUI_TAG} bluesky-emil-gui
